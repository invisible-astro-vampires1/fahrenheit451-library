package fahrenheit451.item;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<ItemData, Long> {
    Optional<ItemData> findById(Long id);
    List<ItemData> findByTitleLikeIgnoreCase(String title);
}

