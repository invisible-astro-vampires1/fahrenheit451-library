package fahrenheit451.item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface ItemHistoryRepository extends JpaRepository<ItemData, Long>  {
    Optional<ItemData> findById(long id);
    List<ItemData> findByTitleLikeIgnoreCase(String title);
}



