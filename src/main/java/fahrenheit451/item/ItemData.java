package fahrenheit451.item;

import fahrenheit451.user.UserData;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.util.Date;
import java.util.Calendar;
import java.util.List;

@Table(name = "items")
@Entity
public class ItemData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;
    @Column
    private String studio;
    @Column
    private String ageRating;
    @Column
    private String type;
    @Column
    private String yearReleased;
    @Column
    private String genre;
    @Column
    private String blurb;
    @Column
    private boolean loaned = false;
    @Column
    private boolean reserved = false;
    @Column
    private boolean returned = false;
    @Column
    private Date dueDate = addTime();
    @Column
    private Date reserveDueDate = addTime();
    @Column
    @UpdateTimestamp
    private Date checkedOut;

    @ManyToOne
    private UserData user;
    @ManyToMany(mappedBy = "itemsHistory")
    List<UserData> userHistory;
    @ManyToOne
    private UserData reservedUser;
    
    public Long getId() {
        return id;
    }

    public Date addTime() {
    	Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.DAY_OF_WEEK, 7);
        return cal.getTime();
    } 
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public String getAgeRating() {
        return ageRating;
    }

    public void setAgeRating(String ageRating) {
        this.ageRating = ageRating;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) { this.type = type; }

    public String getYearReleased() { return yearReleased; }

    public void setYearReleased(String yearReleased) {
        this.yearReleased = yearReleased;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getBlurb() { return blurb; }

    public void setBlurb(String blurb) {
        this.blurb = blurb;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
    	this.dueDate.setTime((checkedOut.getTime() + (7*24*(60*60)*1000)));
        this.dueDate = dueDate;
    }

    public Date getReserveDueDate(){
        return reserveDueDate;
    }

    public void setReserveDueDate(Date reserveDueDate) {
        this.reserveDueDate.setTime((checkedOut.getTime() + (2*(7*24*(60*60)*1000))));
        this.reserveDueDate = reserveDueDate;
    }

    public Date getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(Date checkedOut) {
        this.checkedOut = checkedOut;
    }

    public UserData getUser() { return user; }

    public void setUser(UserData user) { this.user = user; }

    public boolean isLoaned() {
        return loaned;
    }

    public void setLoaned(boolean loaned) {
        this.loaned = loaned;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public boolean isReturned() {
        return returned;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public List<UserData> getUserHistory() {
        return userHistory;
    }

    public void setUserHistory(List<UserData> userHistory) {
        this.userHistory = userHistory;
    }

    public UserData getReservedUser() {
        return reservedUser;
    }

    public void setReservedUser(UserData reservedUser) {
        this.reservedUser = reservedUser;
    }
}
