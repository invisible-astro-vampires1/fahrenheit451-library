package fahrenheit451.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserData, Long>  {
    Optional<UserData> findByEmailAddressAndPassword(String emailAddress, String password);
    List<UserData> findByFirstNameLikeIgnoreCaseOrLastNameLikeIgnoreCase(String firstName,String lastName);
}
