package fahrenheit451.user;

import fahrenheit451.item.ItemData;
import javax.persistence.*;
import java.util.List;

@Table(name = "users")
@Entity
public class UserData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String emailAddress;
    @Column
    private String password;
    @OneToMany(mappedBy = "user")
    private List<ItemData> items;
    @ManyToMany
    private List<ItemData> itemsHistory;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<ItemData> getItems() { return items; }

    public void setItems(List <ItemData> items) { this.items = items; }

    public List<ItemData> getItemsHistory() { return itemsHistory; }

    public void setItemsHistory(List <ItemData> itemsHistory) { this.itemsHistory = itemsHistory; }
}
