package fahrenheit451.controller;
import fahrenheit451.item.ItemData;
import fahrenheit451.item.ItemRepository;
import fahrenheit451.item.ItemHistoryRepository;
import fahrenheit451.librarian.LibrarianData;
import fahrenheit451.librarian.LibrarianRepository;
import fahrenheit451.librarian.LibrarianSession;
import fahrenheit451.user.UserData;
import fahrenheit451.user.UserRepository;
import fahrenheit451.user.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class Fahrenheit451Controller {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LibrarianRepository librarianRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ItemHistoryRepository itemHistoryRepository;
    @Autowired
    private UserSession userSession;
    @Autowired
    private LibrarianSession librarianSession;

    int count = 0;

    @GetMapping("/")
    public String index() {
        if (count==0) {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            UserData user = new UserData();

            user.setFirstName("firstName");
            user.setLastName("lastName");
            user.setEmailAddress("user@admin.com");
            user.setPassword("p");
            user.setItemsHistory(new ArrayList<>());
            userRepository.save(user);

            LibrarianData librarianData = new LibrarianData();

            librarianData.setFirstName("firstName");
            librarianData.setLastName("lastName");
            librarianData.setEmailAddress("user@email.com");
            librarianData.setPassword("p");
            librarianRepository.save(librarianData);

            ItemData item = new ItemData();
            List<UserData> userHistory= new ArrayList<>();
            item.setTitle("title");
            item.setStudio("studio");
            item.setAgeRating("ageRating");
            item.setType("type");
            item.setYearReleased("yearReleased");
            item.setGenre("genre");
            item.setBlurb("blurb");
            item.setUserHistory(userHistory);
            itemRepository.save(item);
            itemHistoryRepository.save(item);

            count++;
//            stmt.close();
//            con.close();
        }

        return "index.html";
    }

    @GetMapping("/register")
    public String register(){ return "register.html"; }

    @GetMapping("/updateMember")
    public String update(){
        if (userSession.getUser() != null) {
            return "memberUpdate.html";
        }
        else {return "redirect:/memberLogin";}
    }
    
    @GetMapping("/librarianUpdateMember")
    public String librarianUpdateMember(Model model,@RequestParam("id") long id) throws SQLException{
        if (librarianSession.getLibrarian() == null){
            return "redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();
            Optional<UserData> optionalUserData = userRepository.findById(id);
            UserData user = optionalUserData.get();
            userSession.setUser(user);
            model.addAttribute("librarian", librarianSession.getLibrarian());
            model.addAttribute("user", user);

//            stmt.close();
//            con.close();

            return "memberUpdate.html";
        }
    }
    
    @GetMapping("/librarianDeleteMember")
    public String librarianDeleteMember(Model model,@RequestParam("id") long id) throws SQLException{
        if (librarianSession.getLibrarian() == null){
            return "redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();
            Optional<UserData> optionalUserData = userRepository.findById(id);
            UserData user = optionalUserData.get();
            userRepository.delete(user);

            model.addAttribute("librarian", librarianSession.getLibrarian());
            model.addAttribute("user", user);

//            stmt.close();
//            con.close();

            return "redirect:/users";
        }
    }
    @GetMapping("/librarianDeleteItem")
    public String librarianDeleteItem(Model model,@RequestParam("id") long id) throws SQLException{
        if (librarianSession.getLibrarian() == null){
            return "redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();
            Optional<ItemData> optionalItemData = itemRepository.findById(id);
            ItemData item = optionalItemData.get();
            itemRepository.deleteById(id);

            model.addAttribute("librarian", librarianSession.getLibrarian());
            model.addAttribute("item", item);

//            stmt.close();
//            con.close();

            return "redirect:/items";
        }
    }
    
    @GetMapping("/registerLibrarian")
    public String registerLibrarian(){
        if (librarianSession.getLibrarian() == null) {
            return "redirect:/librarianLogin";
        } else {
            return "registerLibrarian.html";
        }
    }

    @GetMapping("/librarianHome")
    public String librarianHome(){
        if (librarianSession.getLibrarian() == null){
            return "redirect:/librarianLogin";
        } else {
            userSession.setUser(null);
            LibrarianData librarianData = librarianSession.getLibrarian();
            librarianData.setLoaning(false);
            return "librarianHome.html";
        }
    }

    @GetMapping("/memberHome")
    public String memberHome(Model model){
        if (userSession.getUser() == null){
            return "redirect:/memberLogin";
        } else {
            model.addAttribute("user", userSession.getUser());
            return "memberHome.html";
        }
    }
    
    @GetMapping("/addItem")
    public String addItem(){
        if (librarianSession.getLibrarian() == null){
            return "redirect:/librarianLogin";
        } else {
            return "addItem.html";
        }
    }

    @GetMapping("/memberCurrentLoans")
    public String memberCurrentLoans(Model model,@RequestParam("id") long id) throws SQLException{
        if (userSession.getUser() == null){
            return "redirect:/memberLogin";
        } else {

//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            Optional<UserData> optionalUserData = userRepository.findById(id);
            UserData user = optionalUserData.get();

            model.addAttribute("user", user.getId());
            model.addAttribute("items", user.getItems());
//            stmt.close();
//            con.close();

            return "memberCurrentLoans.html";
        }
    }
    
    @GetMapping("/memberHistoricLoans")
    public String memberHistoricLoans(Model model, @RequestParam(name = "id") long id) throws SQLException{
        if (userSession.getUser() == null){
            return "redirect:/memberLogin";
        } else {

//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            Optional<UserData> optionalUserData = userRepository.findById(id);
            UserData user = optionalUserData.get();

            model.addAttribute("user", user.getId());
            model.addAttribute("items", user.getItemsHistory());
//            stmt.close();
//            con.close();

            return "memberHistoricLoans.html";
        }
    }
    
    @GetMapping("/librarianCurrentLoans")
    public String librarianCurrentLoans(Model model,@RequestParam("id") long id) throws SQLException{
        if (librarianSession.getLibrarian() == null){
            return "redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            Optional<UserData> optionalUserData = userRepository.findById(id);
            UserData user = optionalUserData.get();
            userSession.setUser(user);

            model.addAttribute("librarian", librarianSession.getLibrarian());
            model.addAttribute("items", user.getItems());
            model.addAttribute("user", user);

//            stmt.close();
//            con.close();

            return "memberCurrentLoans.html";
        }
    }

    @GetMapping("/users")
    public String users(Model model) throws SQLException {
        if (librarianSession.getLibrarian() == null) {
            return "redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            model.addAttribute("users", userRepository.findAll());
            model.addAttribute("librarians", librarianRepository.findAll());

//            stmt.close();
//            con.close();

            return "users.html";
        }
    }

    @GetMapping("/usersSearch")
    public String usersSearch(Model model, @RequestParam String name) throws SQLException {
        if (librarianSession.getLibrarian() == null) {
            return "redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            List<UserData> users = userRepository.findByFirstNameLikeIgnoreCaseOrLastNameLikeIgnoreCase("%"+name+"%", "%"+name+"%");
            model.addAttribute("users", users);
            model.addAttribute("librarians", librarianRepository.findAll());

//            stmt.close();
//            con.close();

            return "users.html";
        }
    }

    @GetMapping("/itemSearch")
    public String itemSearch(Model model, @RequestParam String title) throws SQLException {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            List<ItemData> items = itemRepository.findByTitleLikeIgnoreCase("%"+title+"%");
            model.addAttribute("items", items);
            model.addAttribute("user", userSession.getUser());
            model.addAttribute("librarian", librarianSession.getLibrarian());
//            stmt.close();
//            con.close();

            return "items.html";
    }

    @GetMapping("/items")
    public String items(Model model) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();
        LibrarianData librarianData = librarianSession.getLibrarian();

        model.addAttribute("items", itemRepository.findAll());
        model.addAttribute("librarian", librarianData);
        model.addAttribute("user", userSession.getUser());

//        stmt.close();
//        con.close();

        return "items.html";
    }
    
    @GetMapping("/librarianItems")
    public String librarianItems(Model model, long id) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        Optional<UserData> optionalUserData = userRepository.findById(id);
        UserData user = optionalUserData.get();
        userSession.setUser(user);
        LibrarianData librarianData = librarianSession.getLibrarian();
        librarianData.setLoaning(true);

        model.addAttribute("items", itemRepository.findAll());
        model.addAttribute("librarian", librarianSession.getLibrarian());
        model.addAttribute("user", user);
//        stmt.close();
//        con.close();

        return "librarianItems.html";
    }

    @GetMapping("/viewItem")
    public String viewItem(@RequestParam("id") long id, Model model) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        model.addAttribute("items", itemRepository.getOne(id));
        model.addAttribute("user", userSession.getUser());

//        stmt.close();
//        con.close();

        return "viewItem.html";
    }

    @GetMapping("/loanItem")
    public String loanItem(@RequestParam(name = "id") long id) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        UserData user = userSession.getUser();
        LibrarianData librarian = librarianSession.getLibrarian();
        ItemData item = itemRepository.getOne(id);
        ItemData itemHistory = itemHistoryRepository.getOne(id);
        List<ItemData> items = itemRepository.findAll(Example.of(item));

        if (userSession.getUser() == null){
            return "redirect:/memberLogin";
        } else if (!item.isLoaned()){
            user.setItems(items);
            //user.getItems().add(item);
            item.setUser(userSession.getUser());
            //itemHistory.getUserHistory().add(user);
            item.setLoaned(true);
            item.setReturned(false);
            itemRepository.save(item);
            //itemHistoryRepository.save(itemHistory);
            userRepository.save(user);

//            stmt.close();
//            con.close();
            if (librarian != null){
                return "redirect:/librarianHome";
            }else {
                return "redirect:/memberHome";
            }
        } else if(!item.isReserved()){
            item.setReserved(true);
            item.setReservedUser(userSession.getUser());
            itemRepository.save(item);

//            stmt.close();
//            con.close();

            if (librarian != null){
                return "redirect:/librarianHome";
            }else {
                return "redirect:/memberHome";
            }
        } else {
//            stmt.close();
//            con.close();
            return "redirect:/memberHome";
        }
    }

    @GetMapping("/returnItem")
    public String returnItem(@RequestParam(name = "id") long id) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        ItemData item = itemRepository.getOne(id);
        ItemData itemData = itemHistoryRepository.getOne(id);
        List<ItemData> items = itemRepository.findAll(Example.of(item));

        if (userSession.getUser() == null && librarianSession.getLibrarian() == null){
            return "redirect:/memberLogin";
        } else {
            item.setLoaned(false);
            item.setUser(null);
            item.setReturned(true);
            itemRepository.save(item);
            UserData user = item.getReservedUser();

            if (item.isReserved() && !user.getId().equals(userSession.getUser().getId())) {
                List<UserData> userData = userRepository.findAll(Example.of(user));
                user.setItems(items);
                //user.getItemsHistory().add(itemData);
                //itemData.getUserHistory().add(user);
                item.setUser(user);
                item.setLoaned(true);
                item.setReserved(false);
                item.setReservedUser(null);
                item.setReturned(false);
                //item.setUserHistory(userData);
                itemRepository.save(item);
                userRepository.save(user);
                //itemHistoryRepository.save(itemData);
            } else{
                item.setReserved(false);
                item.setReservedUser(null);
                itemRepository.save(item);

            }

//            stmt.close();
//            con.close();

            if (librarianSession.getLibrarian() != null) {
                return "redirect:/librarianHome";
            } else {
                return "redirect:/memberHome";
            }
        }

    }

    @PostMapping("/addItem")
    public String postItem(@RequestParam(name = "title") String title, @RequestParam(name = "studio") String studio,
                           @RequestParam(name = "ageRating") String ageRating, @RequestParam(name = "type") String type,
                           @RequestParam(name = "yearReleased") String yearReleased, @RequestParam(name = "genre") String genre,
                           @RequestParam(name = "blurb") String blurb) throws SQLException {

        if (librarianSession.getLibrarian() == null){
            return"redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            ItemData item = new ItemData();

            item.setTitle(title);
            item.setStudio(studio);
            item.setAgeRating(ageRating);
            item.setType(type);
            item.setYearReleased(yearReleased);
            item.setGenre(genre);
            item.setBlurb(blurb);
            itemRepository.save(item);
            itemHistoryRepository.save(item);

//            stmt.close();
//            con.close();

            return ("redirect:/librarianHome");
        }
    }

    @PostMapping("/register")
    public String postRegister(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName,
                               @RequestParam(name = "emailAddress") String emailAddress, @RequestParam(name = "password") String password) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        UserData user = new UserData();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmailAddress(emailAddress);
        user.setPassword(password);
        userRepository.save(user);
        userSession.setUser(user);

//        stmt.close();
//        con.close();

        return "redirect:/memberHome";
    }

    @PostMapping("/updateMember")
    public String updateMember(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName,
                               @RequestParam(name = "emailAddress") String emailAddress, @RequestParam("password") String password) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        if (userSession.getUser() != null) {
            UserData user = userSession.getUser();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailAddress(emailAddress);
            user.setPassword(password);
            userRepository.save(user);
        }else {
            return "redirect:/memberLogin";
        }
//        stmt.close();
//        con.close();
        
        return "redirect:/memberHome";
    }
    
    @PostMapping("/librarianUpdateMember")
    public String librarianUpdateMemberPost(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName,
            @RequestParam(name = "emailAddress") String emailAddress, @RequestParam("password") String password) throws SQLException{
        if (librarianSession.getLibrarian() == null){
            return "redirect:/librarianLogin";
        } else {
//            Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//            Statement stmt = con.createStatement();

            UserData user = userSession.getUser();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailAddress(emailAddress);
            user.setPassword(password);
            userRepository.save(user);

//            stmt.close();
//            con.close();

            return  "librarianHome.html";
        }
    }

    @PostMapping("/registerLibrarian")
    public String librarianRegister(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName,
                               @RequestParam(name = "emailAddress") String emailAddress, @RequestParam(name = "password") String password) throws SQLException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        LibrarianData librarianData = new LibrarianData();

        librarianData.setFirstName(firstName);
        librarianData.setLastName(lastName);
        librarianData.setEmailAddress(emailAddress);
        librarianData.setPassword(password);
        librarianRepository.save(librarianData);

//        stmt.close();
//        con.close();

        return "librarianHome.html";
    }
}