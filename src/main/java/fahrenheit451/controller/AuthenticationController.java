package fahrenheit451.controller;

import fahrenheit451.librarian.LibrarianData;
import fahrenheit451.librarian.LibrarianRepository;
import fahrenheit451.librarian.LibrarianSession;
import fahrenheit451.user.UserData;
import fahrenheit451.user.UserRepository;
import fahrenheit451.user.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

@Controller
public class AuthenticationController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserSession userSession;
    @Autowired
    LibrarianRepository librarianRepository;
    @Autowired
    LibrarianSession librarianSession;

    @GetMapping("/memberLogin")
    public String memberLogin(Model model) {
        if (userSession.isLoginFailed()) {
            model.addAttribute("error", "Username and Password not correct");
            userSession.setLoginFailed(false);
        }
        return "memberLogin.html";
    }

    @GetMapping("/librarianLogin")
    public String librarianLogin(Model model) {
        if (librarianSession.isLoginFailed()) {
            model.addAttribute("error", "Username and Password not correct");
            librarianSession.setLoginFailed(false);
        }
        return "librarianLogin.html";
    }

    @GetMapping("/librarianLogout")
    public void librarianLogout(HttpServletResponse response) throws Exception {
        librarianSession.setLibrarian(null);
        response.sendRedirect("/");
    }

    @GetMapping("/memberLogout")
    public void logout(HttpServletResponse response) throws Exception {
        userSession.setUser(null);
        response.sendRedirect("/");
    }

    @PostMapping("/memberLogin")
    public void postMember(String emailAddress, String password, HttpServletResponse response) throws SQLException, IOException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        Optional<UserData> user = userRepository.findByEmailAddressAndPassword(emailAddress, password);
        if (user.isPresent()) {
            userSession.setUser(user.get());
            response.sendRedirect("/memberHome");
        } else {
            userSession.setLoginFailed(true);
            response.sendRedirect("/memberLogin");
        }
//        stmt.close();
//        con.close();
    }

    @PostMapping("/librarianLogin")
    public void postLibrarian(String emailAddress, String password,
                              HttpServletResponse response) throws SQLException, IOException {
//        Connection con = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "password");
//        Statement stmt = con.createStatement();

        Optional<LibrarianData> librarian = librarianRepository.findByEmailAddressAndPassword(emailAddress, password);
        if (librarian.isPresent()) {
            librarianSession.setLibrarian(librarian.get());
            response.sendRedirect("/librarianHome");
        } else {
            librarianSession.setLoginFailed(true);
            response.sendRedirect("/librarianLogin");
        }
//        stmt.close();
//        con.close();
    }
}
