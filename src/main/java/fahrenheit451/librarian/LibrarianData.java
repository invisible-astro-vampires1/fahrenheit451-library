package fahrenheit451.librarian;

import javax.persistence.*;

@Table(name = "librarians")
@Entity
public class LibrarianData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String emailAddress;
    @Column
    private String password;
    @Column
    private Boolean loaning;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getEmailAddress() { return emailAddress; }

    public void setEmailAddress(String emailAddress) { this.emailAddress = emailAddress; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Boolean getLoaning() {
        return loaning;
    }

    public void setLoaning(Boolean loaning) {
        this.loaning = loaning;
    }
}
