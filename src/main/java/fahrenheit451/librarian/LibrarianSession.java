package fahrenheit451.librarian;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class LibrarianSession {

    private LibrarianData librarian;
    private boolean loginFailed;

    public LibrarianData getLibrarian() {
        return librarian;
    }

    public void setLibrarian(LibrarianData librarian) { this.librarian = librarian;
    }

    public boolean isLoginFailed() {
        return loginFailed;
    }

    public void setLoginFailed(boolean loginFailed) { this.loginFailed = loginFailed; }
}
