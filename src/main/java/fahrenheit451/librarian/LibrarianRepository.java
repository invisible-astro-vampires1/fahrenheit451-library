package fahrenheit451.librarian;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LibrarianRepository extends JpaRepository<LibrarianData, Long> {
    Optional<LibrarianData> findByEmailAddressAndPassword(String emailAddress, String password);
}
