package fahrenheit451;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fahrenheit451Application {

    public static void main(String[] args)
    {
        SpringApplication.run(Fahrenheit451Application.class, args);
    }

}