FROM openjdk:8-jre-alpine
COPY target/fahrenheit451-0.0.1-SNAPSHOT.jar /fahrenheit451.jar
CMD java -jar fahrenheit451.jar